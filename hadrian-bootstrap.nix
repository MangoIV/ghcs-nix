# Build Hadrian using the GHC's in-built bootstrap logic
{ lib, stdenv, fetchurl, gnutar
, src, version, bootGhc, python3 }:

let
  fetchPlan =
    stdenv.mkDerivation {
      name = "hadrian-${version}-fetch_plan.json";
      inherit src;
      nativeBuildInputs = [ bootGhc python3 ];
      configurePhase = "true";
      buildPhase = ''
        python3 hadrian/bootstrap/bootstrap.py list-sources
      '';
      installPhase = ''
        mkdir -p $out
        cp fetch_plan.json $out/fetch_plan.json
      '';
    };

  sourcesJson = builtins.fromJSON (builtins.readFile "${fetchPlan}/fetch_plan.json");

  sources = lib.mapAttrsToList (name: x: {
    name = name;
    src = fetchurl {
      url = x.url;
      sha256 = x.sha256;
    };
  }) sourcesJson;

  sourcesTarball = stdenv.mkDerivation {
    name = "hadrian-${version}-sources.tar";
    version = version;
    nativeBuildInputs = [ bootGhc gnutar ];
    unpackPhase = "true";
    configurePhase = "true";
    buildPhase =
      lib.concatMapStrings (x: "ln -s ${x.src} ${x.name}\n") sources +
      ''
        ver="$(ghc --print-project-version | sed -e "s/\./_/g")"
        cp ${src}/hadrian/bootstrap/plan-bootstrap-$ver.json plan-bootstrap.json
        tar -cf sources.tar *
      '';
    installPhase = ''
      mv sources.tar $out
    '';
  };

in
stdenv.mkDerivation {
  name = "hadrian-${version}";
  version = version;
  inherit src;
  nativeBuildInputs = [ bootGhc python3 ];
  configurePhase = "true";
  buildPhase = ''
    echo ${sourcesTarball}
    python3 hadrian/bootstrap/bootstrap.py -s ${sourcesTarball}
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp -R _build/bin/hadrian $out/bin/hadrian
  '';
}
